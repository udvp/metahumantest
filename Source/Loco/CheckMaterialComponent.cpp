


#include "CheckMaterialComponent.h"

#include "PhysicalMaterials/PhysicalMaterial.h"

UCheckMaterialComponent::UCheckMaterialComponent()
{
	
	PrimaryComponentTick.bCanEverTick = true;

}


void UCheckMaterialComponent::BeginPlay()
{
	Super::BeginPlay();

	SetComponentTickInterval(0.05f);
	
}


void UCheckMaterialComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	CheckMaterial();

}

void UCheckMaterialComponent::CheckMaterial() const
{
	const FVector StartLocation = GetOwner()->GetActorLocation();
	const FVector EndLocation = StartLocation + GetOwner()->GetActorForwardVector() * 1'000;

	DrawDebugLine(GetWorld(), StartLocation, EndLocation, FColor::Emerald);
	FHitResult HitResult;
	FCollisionQueryParams CollisionParams;
	CollisionParams.bReturnPhysicalMaterial=true;
	CollisionParams.AddIgnoredActor(GetOwner());

	if (GetWorld()->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, CollisionParams))
	{

		if (const UPhysicalMaterial* const PhysMaterial = HitResult.PhysMaterial.Get())
		{
			const FString MaterialName = PhysMaterial->GetName();
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green,
											 FString::Printf(TEXT("Detected Physical Material: %s"), *MaterialName));
		}
	}
}
