

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CheckMaterialComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class LOCO_API UCheckMaterialComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UCheckMaterialComponent();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	void CheckMaterial()const;
};
