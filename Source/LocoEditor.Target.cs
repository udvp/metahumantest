

using UnrealBuildTool;
using System.Collections.Generic;

public class LocoEditorTarget : TargetRules
{
	public LocoEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "Loco" } );
	}
}
