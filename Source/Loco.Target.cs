

using UnrealBuildTool;
using System.Collections.Generic;

public class LocoTarget : TargetRules
{
	public LocoTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "Loco" } );
	}
}
